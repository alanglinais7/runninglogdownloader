# Run these commands to get the required libraries:
# easy_install pip  
# pip install BeautifulSoup4
# pip install requests
import requests
from bs4 import BeautifulSoup
import sys
import re
import os
import os.path
import time
from time import strptime

# constants
RUNNING_LOG_URL = 'http://www.running-log.com'
RUNNING_LOG_LOGIN_URL = RUNNING_LOG_URL + "/athlete/login"
RUNNING_LOG_WORKOUTS_URL = RUNNING_LOG_URL + "/workouts"
RUNNING_LOG_WORKOUT_PAGE_URL = RUNNING_LOG_WORKOUTS_URL + "?page="

USERNAME_FORM_NAME = 'athlete[login]'
PASSWORD_FORM_NAME = 'athlete[password]'
ASSET_DIR_NAME = "assets" # folder for images and css
LOG_DIR_NAME = "logs" # folder where all the logs will be downloaded to


def get_num_pages(session):
    # find out how mnay workoutpages there are
    request_result = session.get(RUNNING_LOG_WORKOUTS_URL)

    # get number of pages
    workouts_page = BeautifulSoup(request_result.text, 'html.parser')
    workout_list = workouts_page.find(id="workout_list")

    if workout_list == None:
        print("Invalid username/password. Or something went wrong.")
        exit(0)

    pagination = workout_list.find("div", {"class": "pagination"})

    if pagination == None:
        num_pages = 1
    else:
        a_tags = pagination.find_all("a")
        num_pages = int(a_tags[len(a_tags)-2].string)

    return num_pages

def make_directories():
    # make directories for files if they don't exist

    if not os.path.exists(ASSET_DIR_NAME):
        os.makedirs(ASSET_DIR_NAME)

    if not os.path.exists(LOG_DIR_NAME):
        os.makedirs(LOG_DIR_NAME)

def get_links_to_workouts(workout_list):
    # get all workouts on the page
    table_rows = workout_list.find_all("tr", {'class':['row_odd', 'row_even']})
    links = []
    for row in table_rows:
        # get first a tag
        link = row.find('a')['href']
        links.append(link)
    return links

def download_css(session, workout_page):
    # download css file if not already
    css_file_name = 'application.css'
    css_path = ASSET_DIR_NAME + "/" + css_file_name
    css_link = workout_page.find("link", {'rel': "stylesheet"})["href"]
    workout_page.find("link", {'rel': "stylesheet"})["href"] = "../" + ASSET_DIR_NAME + "/" + css_link[13:] # change path of the file

    if not os.path.isfile(css_path):
        print("downloading css file...")
        css_request_result = session.get(RUNNING_LOG_URL + '/' + css_link)
        open(css_path, 'wb').write(css_request_result.content)
        print("css file downloaded")

            
def download_running_log_logo(session, workout_page):
    # download running log logo file if not already
    logo_file_name = 'logo_small.png'
    logo_path = ASSET_DIR_NAME + "/" + logo_file_name
    logo_link = workout_page.find("div", {'id': "main_logo"}).a.img["src"]
    workout_page.find("div", {'id': "main_logo"}).a.img["src"] = "../" + ASSET_DIR_NAME + "/" + logo_link[8:] # change path of the file

    if not os.path.isfile(logo_path):
        print("downloading logo file...")
        logo_request_result = session.get(RUNNING_LOG_URL + '/' + logo_link)
        if logo_request_result.status_code == 200:
            with open(logo_path, 'wb') as f:
                for chunk in logo_request_result.iter_content():
                    f.write(chunk)
            print("logo file downloaded")

def construct_log_file_name(workout_page):

    # find spot in the page whhere the log title is
    interior_wrapper = workout_page.find("div", {'class': 'interior_wrapper'})
    log_title_spot = interior_wrapper.find("h3")
    log_title = log_title_spot.string.strip()
    
    def convert_date(date_str):
         # convert the date from something like "May 26, 2018 (Afternoon)" to "2018-05-26(A)"
        date = date_str.split() # [month, day,, year, (time of day)]
        date[0] = date[0][0:3] # abbreviate month
        month_num = strptime(date[0],'%b').tm_mon
        if month_num < 10:
            month_num = "0" + str(month_num)
        day_num = date[1][:-1]
        if int(day_num) < 10:
            day_num = "0" + str(day_num)
        year = date[2]

        # add on the first letter for time of day
        time_of_day = date[3][0:2] + ")"
        date = "{}-{}-{}{}".format(year, month_num, day_num, time_of_day)
        return date

    # find the date and the exercise type
    date_str = log_title_spot.next_sibling.next_sibling
    exercise_type_str = log_title_spot.next_sibling.next_sibling.next_sibling.next_sibling
    
    # strip of white space on the ends
    date_str =  date_str.string.strip()
    exercise_type_str = exercise_type_str.string.strip()

    
    date = convert_date(date_str)


    # parse the exercise type
    exercise_type_colon_index = exercise_type_str.find(':')
    exercise_type = exercise_type_str[exercise_type_colon_index+2:]

    # remove invalid file name characters
    title = re.sub(r'[\\/*?:"<>|]',"", log_title) # remove invalid characters for filename

    # combine each element into example: "2018-05-26(A)_Run_My dank title.html"
    file_name = "{}_({})_{}.html".format(date, exercise_type, title)

    return file_name

def clean_page(workout_page):
    # remove unneeded content
    # since these are static pages with links that don't work, we want to clear a lot of it up
    workout_page.find("div", {"class": "clearFix"}).extract() # remove header stuff (My Log, Teams, Store)
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").next_sibling.next_sibling.next_sibling.next_sibling.extract() # remove p tag for editing log
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").next_sibling.next_sibling.extract() # remove p tag for deleting log
    workout_page.find("div", {"class": "interior_wrapper"}).find("h2").extract() # remove View Workout

    # disable all links
    for a in workout_page.find_all("a"):
        a['href'] = "#"


def save_page(workout_page, file_name):
    # save the content of the page on your drive
    log_page = workout_page.prettify()
    file = open(LOG_DIR_NAME + "/" + file_name,"w+") 
    file.write(log_page) 
    file.close()
    print("Downloaded: \"{}\"".format(file_name))

def main():
    start_time = time.time()
    args = sys.argv

    if len(args) < 3:
        print("Error: You must supply your username and password as arguments!")
        print("How to run this program on command line: python running_log_downloader.py <username> <password>")
        print("Obviously don't use the <> symbols with your username and password")
        exit(0)

    username = args[1]
    password = args[2]

    # Start a session so we can stay logged in
    session = requests.Session()

    # This is the form data that the page sends when logging in
    login_data = {
        USERNAME_FORM_NAME: username,
        PASSWORD_FORM_NAME: password,
    }

    # login
    request_result = session.post(RUNNING_LOG_LOGIN_URL, data=login_data)
    # TODO: check that login worked

    # find out how many pages of workouts there are
    num_pages = get_num_pages(session)

    # make directories for better organization of the files
    make_directories()

    # counter for num logs downloaded. for fun
    num_logs = 0
    num_pages_left = num_pages

    # process each workout page
    while num_pages_left > 0:

        # navigate to the workout page
        request_result = session.get(RUNNING_LOG_WORKOUT_PAGE_URL + str(num_pages_left))
        workouts_page = BeautifulSoup(request_result.text, 'html.parser')
        workout_list = workouts_page.find(id="workout_list")
        links = get_links_to_workouts(workout_list)

        # reverse the links so that we start with the oldest log
        links = links[::-1]

        for link in links: # for each log

            # send request to server to get html file
            request_result = session.get(RUNNING_LOG_URL + link)

            # get page for parsing from BeautifulSoup
            workout_page = BeautifulSoup(request_result.text, 'html.parser')

            download_css(session, workout_page)
            download_running_log_logo(session, workout_page)

            file_name = construct_log_file_name(workout_page)
            
            clean_page(workout_page)

            save_page(workout_page, file_name)
            num_logs+=1

        num_pages_left -= 1

    print()
    print("Num logs downloaded: {}".format(num_logs))
    print("Time elapsed: {} seconds".format(time.time() - start_time))

if __name__ == '__main__':
    main()
