RunningLogDownloader

This is a Python tool for downloading all your logs on running-log.com.

Requirements:
1. Have python3 installed pon your machine. You can do that here: https://www.python.org/downloads/
2. Install BeautifulSoup. This  can be done by running the command "pip install BeautifulSoup4" on your command line.
3. Install requests. This  can be done by running the command "pip install requests" on your command line.
4. Have an account on running-log.com

How to use:
First download the the file in this repositionry "running_log_downloader.py" into the directory where you want all your logs saved. Then, open a terminal in this directory and run the command "python running_log_downloader.py <email> <password>" where email and password are your credentials for running-log.com. Obviously, do not include the <> symbols.

This script will go back in your log history and download every log that you have posted - starting with your oldest ones - as html files. You will see in a file explorer that two folders will have been made in the directory where you saved this script: "logs" and "assets". "logs" is the folder where all your logs are to be saved, and "assets" just contains the running-log logo and a css file. The assets are included to make the pages that are downloaded look a little nicer.


File names for your logs follow the following format: DATE(Time of Day)_(Exercise Type)_TITLE.html. Example: "2017-03-05(M)_(Run)_Attempt to run through injury.html". The date is in the form YYYY-MM-DD, and the Time of Day is either M (morning), A (afternoon),  or N (Night). Addtionally, all invalid file name characters were removed from log titles.

Upon each run of the script, files will be overwritten. None will be deleted.

Known issues:
This script does not do much error checking or error handling. running-log.com is known to have issues, and this may impact you when you run the script.
